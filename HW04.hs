{-# OPTIONS_GHC -Wall #-}
module HW04 where

import Data.List

newtype Poly a = P [a]

-- Exercise 1 -----------------------------------------

x :: Num a => Poly a
x = P [0, 1]

-- Exercise 2 ----------------------------------------

instance (Num a, Eq a) => Eq (Poly a) where
    P a == P b = strip a == strip b
      where strip = dropWhileEnd (==0)
 
-- Exercise 3 -----------------------------------------

instance (Num a, Eq a, Show a) => Show (Poly a) where
    show (P [])  = "0"
    show (P [0]) = "0"
    show (P cs)  = intercalate " + " $ reverse $ filter (/="") $ zipWith term cs [0..]
      where
        coef :: (Num a, Eq a, Show a) => a -> String
        coef   1  = ""
        coef (-1) = "-"
        coef   c  = show c

        term :: (Num a, Eq a, Show a) => a -> Int -> String
        term 0 _ = ""
        term c 0 = show c
        term c 1 = coef c ++ "x"
        term c e = coef c ++ "x^" ++ show e

-- Exercise 4 -----------------------------------------

plus :: Num a => Poly a -> Poly a -> Poly a
plus (P xs) (P ys) = P $ plusL xs ys
  where plusL [] bs = bs
        plusL as [] = as
        plusL (a:as) (b:bs) = a + b : plusL as bs

-- Exercise 5 -----------------------------------------

times :: Num a => Poly a -> Poly a -> Poly a
times (P p1) (P p2) = foldr f (P [0]) $ zip p1 [0..]
  where f = (+) . (\ (c, s) -> P $ replicate s 0 ++ map (*c) p2)

-- Exercise 6 -----------------------------------------

instance Num a => Num (Poly a) where
    (+) = plus
    (*) = times
    negate (P cs) = P $ map negate cs
    fromInteger = P . (:[]) . fromInteger
    -- No meaningful definitions exist
    abs    = undefined
    signum = undefined

-- Exercise 7 -----------------------------------------

applyP :: Num a => Poly a -> a -> a
applyP (P cs) m = sum $ zipWith (\ c e -> c * m^e) cs ([0..] :: [Int])

-- Exercise 8 -----------------------------------------

class Num a => Differentiable a where
    deriv  :: a -> a
    nderiv :: Int -> a -> a
    nderiv n y
      | n <= 0    = y
      | otherwise = nderiv (n-1) $ deriv y

-- Exercise 9 -----------------------------------------

instance Num a => Differentiable (Poly a) where
    deriv (P [])     = P []
    deriv (P [_])    = P [0]
    deriv (P (_:cs)) = P $ zipWith (*) cs $ map fromInteger [1..]
