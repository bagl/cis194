-- CIS 194, Spring 2015
--
-- Test cases for HW 01

module HW01Tests where

import HW01
import Testing

-- Exercise 1 -----------------------------------------

testLastDigit :: (Integer, Integer) -> Bool
testLastDigit (n, d) = lastDigit n == d

testDropLastDigit :: (Integer, Integer) -> Bool
testDropLastDigit (n, d) = dropLastDigit n == d

ex1Tests :: [Test]
ex1Tests = [ Test "lastDigit test" testLastDigit
             [(123, 3), (1234, 4), (5, 5), (10, 0), (0, 0)]
           , Test "dropLastDigit test" testDropLastDigit
             [(123, 12), (1234, 123), (5, 0), (10, 1), (0,0)]
           ]

-- Exercise 2 -----------------------------------------

testToRevDigits :: (Integer, [Integer]) -> Bool
testToRevDigits (n, d) = toRevDigits n == d

ex2Tests :: [Test]
ex2Tests = [ Test "toRevDigits test" testToRevDigits
             [ (-100, []), (-1, []), (0, []), (1, [1])
             , (9, [9]), (10, [0,1]), (29, [9,2])
             , (123, [3,2,1]), (4921, [1,2,9,4])
             ]
           ]

-- Exercise 3 -----------------------------------------

testDoubleEveryOther :: ([Integer], [Integer]) -> Bool
testDoubleEveryOther (xs, ys) = doubleEveryOther xs == ys

ex3Tests :: [Test]
ex3Tests = [ Test "doubleEveryOther test" testDoubleEveryOther
             [ ([], []), ([1], [1]), ([1,2], [1,4])
             , ([1,2,3,4,5], [1,4,3,8,5])
             , ([1,2,3,4,5,6], [1,4,3,8,5,12])
             ]
           ]

-- Exercise 4 -----------------------------------------

testSumDigits :: ([Integer], Integer) -> Bool
testSumDigits (xs, s) = sumDigits xs == s

ex4Tests :: [Test]
ex4Tests = [ Test "sumDigits test" testSumDigits
             [ ([], 0), ([1], 1), ([1,2,3], 6)
             , ([20, 99], 20), ([100, 32, 5], 11)
             ]
           ]

-- Exercise 5 -----------------------------------------

testLuhn :: (Integer, Bool) -> Bool
testLuhn (d, s) = luhn d == s

ex5Tests :: [Test]
ex5Tests = [ Test "luhn test" testLuhn
             [ (5594589764218858, True)
             , (1234567898765432, False)
             , (4485783828667748, True)
             , (4916725318777049, True)
             , (4916222638521224, True)
             , (4916312881027738, True)
             , (4532549377233361, True)
             , (5368966421698108, True)
             , (5336360460842901, True)
             , (5495305366802373, True)
             , (5577689700425731, True)
             , (5451470408054276, True)
             , (6011105119241520, True)
             , (6011368213520435, True)
             , (6011636371172870, True)
             , (6011303988705192, True)
             , (6011289765139830, True)
             , (347152906353892, True)
             , (379363242549504, True)
             , (343722733047067, True)
             , (343409128691374, True)
             , (375805799063390, True)
             ]
           ]

-- Exercise 6 -----------------------------------------

testHanoi :: (Integer, Peg, Peg, Peg, [Move]) -> Bool
testHanoi (n,a,b,c,ms) = hanoi n a b c == ms

ex6Tests :: [Test]
ex6Tests = [ Test "hanoi Test" testHanoi
             [ (1, "a", "b", "c", [("a","c")])
             , (2, "a", "b", "c", [("a","b"), ("a","c"), ("b","c")])
             ]
           ]

-- All Tests ------------------------------------------

allTests :: [Test]
allTests = concat [ ex1Tests
                  , ex2Tests
                  , ex3Tests
                  , ex4Tests
                  , ex5Tests
                  , ex6Tests
                  ]
