{-# OPTIONS_GHC -Wall #-}
module HW02 where

-- Mastermind -----------------------------------------

-- A peg can be one of six colors
data Peg = Red | Green | Blue | Yellow | Orange | Purple
         deriving (Show, Eq, Ord)

-- A code is defined to simply be a list of Pegs
type Code = [Peg]

-- A move is constructed using a Code and two integers; the number of
-- exact matches and the number of regular matches
data Move = Move Code Int Int
          deriving (Show, Eq)

-- List containing all of the different Pegs
colors :: [Peg]
colors = [Red, Green, Blue, Yellow, Orange, Purple]

-- Exercise 1 -----------------------------------------

-- Get the number of exact matches between the actual code and the guess
exactMatches :: Code -> Code -> Int
exactMatches xs ys = sum $ zipWith (\x y -> if x == y then 1 else 0) xs ys

-- Exercise 2 -----------------------------------------

-- For each peg in xs, count how many times is occurs in ys
countColors :: Code -> [Int]
countColors cs = map (\x -> length $ filter (x==) cs) colors

-- Count number of matches between the actual code and the guess
matches :: Code -> Code -> Int
matches xs ys = sum $ zipWith min (countColors xs) (countColors ys)

-- Exercise 3 -----------------------------------------

-- Construct a Move from a guess given the actual code
getMove :: Code -> Code -> Move
getMove code guess = Move guess exact inexact
  where
    exact   = exactMatches code guess
    inexact = matches code guess - exact

-- Exercise 4 -----------------------------------------

isConsistent :: Move -> Code -> Bool
isConsistent (Move code e i) guess = e == ex && i == ix
  where (Move _ ex ix) = getMove code guess

-- Exercise 5 -----------------------------------------

filterCodes :: Move -> [Code] -> [Code]
filterCodes = filter . isConsistent

-- Exercise 6 -----------------------------------------

allCodes :: Int -> [Code]
allCodes n
  | n <= 0    = []
  | n == 1    = map (:[]) colors
  | otherwise = p1 $ allCodes $ n - 1
  where
    p1 :: [Code] -> [Code]
    p1 = concatMap (\code -> map (:code) colors)

-- Exercise 7 -----------------------------------------

solve :: Code -> [Move]
solve secret = reverse $ fst $ go (replicate n Red) [] $ allCodes n
  where
    n = length secret
    go :: Code -> [Move] -> [Code] -> ([Move], [Code])
    go code moves codes
      | code == secret = (newMove : moves, [])
      | otherwise      =
        let cCodes  = filterCodes newMove codes
        in case cCodes of
          []    -> ([], [])
          (c:_) -> go c (newMove : moves) cCodes
      where newMove = getMove secret code

-- Bonus ----------------------------------------------

fiveGuess :: Code -> [Move]
fiveGuess = undefined
